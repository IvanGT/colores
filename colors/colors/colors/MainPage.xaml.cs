﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace colors
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}


        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            if (sender == id_sl_red)
            {
               valueRed.Text = "" + (int)id_sl_red.Value;
            }
            else if (sender == id_sl_green)

            {
                valueGreen.Text = "" + (int)id_sl_green.Value;
            }
            else if (sender == id_sl_blue)
            {
                valueBlue.Text = "" + (int)id_sl_blue.Value;
            }
            else if (sender == id_sl_alpha) {
                valueBold.Text = "" + (int)id_sl_alpha.Value;
            }

                /*if(sender == id_sl_blue)
                 {
                     DisplayAlert("Error", "Prueba De Colores", "Ok");
                 }*/
                id_vd_background.BackgroundColor = Color.FromRgba((int)id_sl_red.Value,(int)id_sl_green.Value,(int)id_sl_blue.Value,(int)id_sl_alpha.Value);
        }
        async private void Buttom (object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Cuadro());
        }
	}
}
